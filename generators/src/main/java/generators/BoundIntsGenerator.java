package generators;

public class BoundIntsGenerator {
	
	private int generateNumber;
	private int max;
	
	public BoundIntsGenerator(int min, int max) {
		generateNumber = min;
		this.max = max;
	}


	public int generate() {
		return generateNumber++;	
	}
	
	public boolean hasNext() {
		return generateNumber <= max;
	}
	
	public static void main(String[] args) {
		BoundIntsGenerator generator = new BoundIntsGenerator(6,10);
		
		while(generator.hasNext()) {
			System.out.println(generator.generate());
		}
		
	}
	
}
