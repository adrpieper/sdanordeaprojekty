package generators.iter;

public class WordGenerator {

	private RandomGenerator wordLenghtGenerator;
	private CharGenerator charGenerator;
	
	public WordGenerator(int minWordLenght, int maxWordLenght) {
		wordLenghtGenerator = new RandomGenerator(minWordLenght, maxWordLenght);
		charGenerator = new CharGenerator();
	}
	
	public String generate() {
		
		int lenght = wordLenghtGenerator.generate();
		String word = "";
		
		for(int i = 0 ; i < lenght ; i++) {
			word += charGenerator.generate();
		}
		
		return word;
	}
	
	public static void main(String[] args) {

		WordGenerator wordGenerator = new WordGenerator(8, 10);
		for(int i = 0 ; i < 10 ; i++) {
			System.out.println(wordGenerator.generate());
		}
		
	}

}
