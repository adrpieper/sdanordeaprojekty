package generators.iter;

import java.io.Serializable;
import java.util.Iterator;

public class FiveGenerator implements Iterable<Integer> {

	public Iterator<Integer> iterator() {
		return new Iterator<Integer>() {

			int couter = 0;
			public boolean hasNext() {
				return couter<5;
			}

			public Integer next() {
				couter++;
				return 5;
			}
			
		};
	}
	
	public static void main(String[] args) {
		
		FiveGenerator generator = new FiveGenerator(); 
		
		for(Iterator<Integer> it = generator.iterator() ; it.hasNext() ; ){
			System.out.println(it.next());
		}
		
		Iterator<Integer> it = generator.iterator(); // int couter = 0;
		while(it.hasNext()) { // while(counter < 5)
			System.out.println(it.next()); // counter++ sysout(5)
		}
				
		
		
		for(int i : generator) {
			System.out.println(i);
		}

		for(int i : generator) {
			System.out.println(i);
		}

		for(int i : generator) {
			System.out.println(i);
		}
		
	}
	
}
