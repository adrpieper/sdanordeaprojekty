package generators.iter;

public class TrueFalseGenerator {
	private boolean value = false;
	
	public boolean generate(){
		return value = !value;
	}
	
	public static void main(String[] args) {
		TrueFalseGenerator generator = new TrueFalseGenerator();
		
		for(int i = 0; i < 10; i++){
			System.out.println(generator.generate());
		}
	}
}

