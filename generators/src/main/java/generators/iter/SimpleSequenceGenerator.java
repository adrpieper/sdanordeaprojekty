package generators.iter;

import java.util.Iterator;

public class SimpleSequenceGenerator implements Iterable<Integer>{

	private final int amount;
	
	public SimpleSequenceGenerator(int amount) {
		this.amount = amount;
	}

	public Iterator<Integer> iterator() {
		return new SimpleSequenceIterator();
	}
	
	class SimpleSequenceIterator implements Iterator<Integer> {
		private int generateNumber = 0;

		public boolean hasNext() {
			return generateNumber < amount;
		}

		public Integer next() {
			generateNumber++;
			return generateNumber;	
		}
	}
	
	public static void main(String[] args) {
		
		SimpleSequenceGenerator generator = new SimpleSequenceGenerator(12);
		for(int i : generator) {
			System.out.println(i);
		}
	}




}
