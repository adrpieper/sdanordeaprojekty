package generators.iter;

import java.util.Iterator;
import java.util.Random;

public class RandomCharGenerator implements Iterable<Character>{

	private int amount;
	
	public RandomCharGenerator(int amount) {
		this.amount = amount;
	}

	public Iterator<Character> iterator() {
		return new RandomCharIterator();
	}	
	
	class RandomCharIterator implements Iterator<Character> {
		private Random random = new Random(11);
		
		private int counter = 0;
		private static final int RANGE = 'z' - 'a';
		
		public boolean hasNext() {
			return counter < amount;
		}

		public Character next() {
			counter++;
			return (char)(random.nextInt(RANGE)+'a');
		}
		
	}
	
	public static void main(String[] args) {

		RandomCharGenerator generator = new RandomCharGenerator(5);
		
		for(char i : generator) {
			System.out.println(i);
		}
		
		for(char i : generator) {
			System.out.println(i);
		}

		
	}
}
