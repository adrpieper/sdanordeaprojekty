package generators;

public class EvenIntsGenerator {
	int evenNumber = 0;

	public int generate() {
		evenNumber = evenNumber + 2;
		return evenNumber;
	}

	public static void main(String[] args) {

		EvenIntsGenerator generator = new EvenIntsGenerator();

		for (int i = 0; i < 10; i++) {
			System.out.println(generator.generate());
		}
	}

}
