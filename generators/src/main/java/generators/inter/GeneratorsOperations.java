package generators.inter;

public class GeneratorsOperations {

	public static void printTenElements(Generator generator) {
		for (int i = 0; i < 10; i++) {
			System.out.println(generator.generate());
		}
	}

	public static int sum(Generator<Integer> generator) {
		int sum = 0;
		
		while(true) {
			Integer i = generator.generate();
			if (i != null) {
				sum+=i;
			}
			else {
				break;
			}
		}
		return sum;
	}

	public static int sum(Generator<Integer> generator, int amount) {
		int sum = 0;

		for (int i = 0; i < amount; i++) {
			sum += generator.generate();
		}

		return sum;
	}
}
