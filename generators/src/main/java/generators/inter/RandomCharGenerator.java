package generators.inter;

import java.util.Random;

import generators.inter.Generator;
import generators.inter.GeneratorsOperations;

public class RandomCharGenerator implements Generator<Character>{

	private Random random = new Random(11);
	
	private static final int RANGE = 'z' - 'a';
	
	public Character generate() {
		return (char)(random.nextInt(RANGE)+'a');
	}
	
	public static void main(String[] args) {

		RandomCharGenerator generator = new RandomCharGenerator();
		
		GeneratorsOperations.printTenElements(generator);
	}

}
