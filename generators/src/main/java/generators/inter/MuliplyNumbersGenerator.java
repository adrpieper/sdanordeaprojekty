package generators.inter;

public class MuliplyNumbersGenerator implements Generator<Integer>{
	private int number;
	private int nextNumber;
	
	public MuliplyNumbersGenerator(int number) {
		this.number = number;
	}

	
	public Integer generate() {
		nextNumber += number;
		return nextNumber;
	}

	public static void main(String[] args) {

		GeneratorsOperations.printTenElements(new MuliplyNumbersGenerator(5));
	}

}
