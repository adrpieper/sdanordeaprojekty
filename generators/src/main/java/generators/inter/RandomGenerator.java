package generators.inter;

import java.util.Random;

import generators.inter.Generator;

public class RandomGenerator implements Generator<Integer> {

	private Random random = new Random();
	private int min;
	private int range;
	
	public RandomGenerator(int min, int max) {
		this.min = min;
		range = max-min+1;
	}

	public Integer generate() {
		return random.nextInt(range) + min;
	}
	
	public static void main(String[] args) {

		RandomGenerator generator = new RandomGenerator(1,3);
		
		for(int i = 0 ; i < 10 ; i++) {
			System.out.println(generator.generate());
		}
	}

}
