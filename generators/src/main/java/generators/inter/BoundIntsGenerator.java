package generators.inter;

import java.util.List;

public class BoundIntsGenerator implements Generator<Integer>{
	
	private int generateNumber;
	private int max;
	
	public BoundIntsGenerator(int min, int max) {
		generateNumber = min;
		this.max = max;
	}


	public Integer generate() {
		if (hasNext())
			return generateNumber++;	
		return null;
	}
	
	public boolean hasNext() {
		return generateNumber <= max;
	}
	
	public static void main(String[] args) {
		GeneratorsOperations.printTenElements(new BoundIntsGenerator(6,10));
		System.out.println(GeneratorsOperations.sum(new BoundIntsGenerator(1, 3)));
	}
	
}
