package generators.inter;

public interface Generator<GeneratedValueType> {

	GeneratedValueType generate();
}
