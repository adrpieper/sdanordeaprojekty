package generators.inter;

public class EvenIntsGenerator implements Generator<Integer> {
	int evenNumber = 0;

	public Integer generate() {
		evenNumber = evenNumber + 2;
		return evenNumber;
	}

	public static void main(String[] args) {

		GeneratorsOperations.printTenElements(new EvenIntsGenerator());
		System.out.println(GeneratorsOperations.sum(new EvenIntsGenerator(), 5));

	}
}
