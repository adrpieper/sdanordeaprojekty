package generators.inter;

public class CharGenerator implements Generator<Character> {
	private char c = 'a';
	
	public Character generate(){
		if (c > 'z') {
			c = 'a';
		}
		return c++;
	}
	
	public static void main(String[] args) {
		CharGenerator generator = new CharGenerator();
		
		for (int i = 0; i < 100; i++) {
			System.out.println(generator.generate());
		}
		
		
	}
}
