package generators.inter;

public class WordGenerator implements Generator<String> {

	private Generator<Integer> wordLenghtGenerator;
	private Generator<Character> charGenerator;
	
	public WordGenerator(Generator<Integer> wordLenghtGenerator, Generator<Character> charGenerator) {
		this.wordLenghtGenerator = wordLenghtGenerator;
		this.charGenerator = charGenerator;
	}

	public WordGenerator(int minWordLenght, int maxWordLenght) {
		wordLenghtGenerator = new RandomGenerator(minWordLenght, maxWordLenght);
		charGenerator = new CharGenerator();
	}
	
	public String generate() {
		
		int lenght = wordLenghtGenerator.generate();
		String word = "";
		
		for(int i = 0 ; i < lenght ; i++) {
			word += charGenerator.generate();
		}
		
		return word;
	}
	
	public static void main(String[] args) {

		WordGenerator wordGenerator = new WordGenerator(new EvenIntsGenerator(),new  CharGenerator());
		GeneratorsOperations.printTenElements(wordGenerator);
		
	}

}
