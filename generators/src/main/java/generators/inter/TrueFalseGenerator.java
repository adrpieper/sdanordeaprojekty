package generators.inter;

public class TrueFalseGenerator implements Generator<Boolean> {
	private boolean value = false;
	
	public Boolean generate(){
		return value = !value;
	}
	
	public static void main(String[] args) {
				
		GeneratorsOperations.printTenElements(new TrueFalseGenerator());
	}
}

