package generators.inter;

public class SimpleSequenceGenerator implements Generator<Integer> {

	private int generateNumber = 0;
	
	public Integer generate() {
		generateNumber++;
		return generateNumber;	
	}
	
	public static void main(String[] args) {
		GeneratorsOperations.printTenElements(new SimpleSequenceGenerator());
	}
}
