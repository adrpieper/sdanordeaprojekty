package generators;

import java.util.Random;

public class RandomCharGenerator {

	private Random random = new Random(11);
	
	private static final int RANGE = 'z' - 'a';
	
	public char generate() {
		return (char)(random.nextInt(RANGE)+'a');
	}
	
	public static void main(String[] args) {

		RandomCharGenerator generator = new RandomCharGenerator();
		
		for(int i = 0 ; i < 10 ; i++) {
			System.out.println(generator.generate());
		}
	}

}
