package generators;

public class MuliplyNumbersGenerator {
	private int number;
	private int nextNumber;
	
	public MuliplyNumbersGenerator(int number) {
		this.number = number;
	}

	
	public int generate() {
		nextNumber += number;
		return nextNumber;
	}

	public static void main(String[] args) {

		MuliplyNumbersGenerator generator = new MuliplyNumbersGenerator(5);

		for (int i = 0; i < 10; i++) {
			System.out.println(generator.generate());
		}
	}

}
