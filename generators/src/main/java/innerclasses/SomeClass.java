package innerclasses;

public class SomeClass {

	int a = 5;
	
	public static void main(String[] args) {
		System.out.println("HelloWorld");
		SomeClass someClass = new SomeClass();
		someClass.a= 100;
		someClass.showData();
		SomeClass someClass1 = new SomeClass();
		someClass1.showData();

		OtherClass otherClass = someClass.createObject();
		OtherClass otherClass1 = someClass1.createObject();

		new OtherStaticClass();
		otherClass.showData();
		otherClass1.showData();
	}
	
	public void showData() {
		System.out.println("a=" + a);
	
	}
	
	public OtherClass createObject() {
		return new OtherClass();
	}
	
	
	public class OtherClass {
		int b = 6;
		
		public void showData() {
			System.out.println("b=" + this.b + ", a="+SomeClass.this.a);
		}	
	}	
	
	
	public static class OtherStaticClass {
		
	}
}

