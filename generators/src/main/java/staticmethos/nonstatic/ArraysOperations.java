package staticmethos.nonstatic;

public class ArraysOperations {

	public static void main(String[] args) {
		int[] data = {1,2,3};
		System.out.println(sum(data));
	}
	
	public static int sum(int[] numbers) {
		int sum = 0;
		
		for(int i : numbers) {
			sum += i;
		}
		return sum;
	}
}
