package staticmethos.nonstatic;

public class ArrayOfNumbers2 {
	private int[] numbers;

	public ArrayOfNumbers2(int... numbers){
		this.numbers = numbers;
	}
	
	public static void main(String[] args) {
		ArrayOfNumbers2 newObject = new ArrayOfNumbers2(1,2,3);
		System.out.println(newObject.sum());
	}
	
	public int sum(){
		int sum = 0;
		
		for(int i : numbers) {
			sum += i;
		}
		return sum;
	}
}
