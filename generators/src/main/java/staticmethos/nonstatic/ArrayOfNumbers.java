package staticmethos.nonstatic;

public class ArrayOfNumbers {
	private int[] numbers = {1,2,3};

	public static void main(String[] args) {
		ArrayOfNumbers newObject = new ArrayOfNumbers();
		System.out.println(newObject.sum());
	}
	
	public int sum(){
		int sum = 0;
		
		for(int i : numbers) {
			sum += i;
		}
		return sum;
	}
}
