package memory;

public class IntegerExample {

	public static void main(String[] args) {
		Integer a = 5; // new Integer(5);
		Integer b = 6; // new Integer(6);
		a = b;
		b = 10; // new Integer(10);
		System.out.println(a);
		System.out.println(b);
		
	}

}
