package memory;

public class MutableIntegerExample {

	public static void main(String[] args) {
		MutableInteger a = new MutableInteger(5);
		MutableInteger b = new MutableInteger(6);
		a = b;
		b.setValue(10);
		System.out.println(a.getValue());
		System.out.println(b.getValue());

	}
}
