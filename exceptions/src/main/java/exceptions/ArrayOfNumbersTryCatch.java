package exceptions;

import java.util.Scanner;

public class ArrayOfNumbersTryCatch {

	
	public static void main(String[] args) {
		int[] numbers = {0,5,10};
		Scanner  scanner = new Scanner(System.in);
		
		do {	
			System.out.println("Podaj Liczbe");
			int index = scanner.nextInt();

			try {
				System.out.println("Liczba na indeksie " + index + " to " + numbers[index]);
				break;
			}catch(ArrayIndexOutOfBoundsException exception) {
				System.out.println("Indeks poza zakresem.");
			}			
			
		}while(true);
		
	}
}
