package exceptions;

import java.util.Scanner;

public class ArrayOfNumbers {

	
	public static void main(String[] args) {
		int[] numbers = {0,5,10};
		Scanner  scanner = new Scanner(System.in);
		
		do {	
			System.out.println("Podaj Liczbe");
			int index = scanner.nextInt();
			if (index < numbers.length && index >= 0) {
				System.out.println("Liczba na indeksie " + index + " to " + numbers[index]);
				break;
			}
			else {
				System.out.println("Indeks poza zakresem.");
			}
		}while(true);
		
	}
}
