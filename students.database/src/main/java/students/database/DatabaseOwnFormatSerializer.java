package students.database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DatabaseOwnFormatSerializer implements DatabaseSerializer {

	@Override
	public void save(StudentsDatabase database) {
		try {
			PrintStream out = new PrintStream("studenci-myformat.txt");
			for(Student student : database.getAll()) {
				out.println(student.getName() + " " + student.getSurname() + " " + student.getIndexNumber());
			}
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	@Override
	public StudentsDatabase load() {
		
		try {
			Scanner scanner = new Scanner(new File("studenci-myformat.txt"));
			List<Student> students = new ArrayList<Student>();
			while (scanner.hasNext()) {
				String name = scanner.next();
				String surname = scanner.next();
				long index = scanner.nextLong();
				Student student = new Student(index, name, surname);
				students.add(student);
			}
			scanner.close();
			return new StudentsDatabase(students);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
