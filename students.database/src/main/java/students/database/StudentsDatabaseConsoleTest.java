package students.database;

public class StudentsDatabaseConsoleTest {

	
		public static void main(String[] args) {
			DatabaseSerializer databaseSerializator = new DatabaseBinarySerialiser();
			
			StudentsDatabase database = new StudentsDatabase();
			database.add("Jan", "Kowalski");
			
			for(int i = 0 ; i < 1000 ; i++) {
				database.add("Adam", "Nowak");
			}
		
			databaseSerializator.save(database);
			
			StudentsDatabase loaded = databaseSerializator.load();
			
			for (Student student : loaded.getAll()) {
				System.out.println(student.getName() + " " + student.getIndexNumber());
			}
			
		}
}
