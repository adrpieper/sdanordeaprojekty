package students.database;

public interface DatabaseSerializer {

	void save(StudentsDatabase database);

	StudentsDatabase load();

}