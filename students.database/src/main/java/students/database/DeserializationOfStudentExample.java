package students.database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializationOfStudentExample {

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		// TODO Auto-generated method stub

		System.out.println("OK 1");
		try {
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("student.txt"));
			System.out.println("OK 2");
			Student s = (Student) inputStream.readObject();
	
			System.out.println(s.getIndexNumber());
			System.out.println(s.getName());
			System.out.println(s.getSurname());
			
		}
	    catch (FileNotFoundException exception) {
			System.out.println("OK 3");
	    }
		try {
			int[] liczby = {1,2};
			
			System.out.println(liczby[100]);
		}catch(IndexOutOfBoundsException e) {
			System.out.println("Wszystko OK");
		}
	}

}
