package students.database;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class DatabaseNativeSerializer implements DatabaseSerializer {
	
	@Override
	public void save(StudentsDatabase database) {
		ObjectOutputStream stream;
		try {
			stream = new ObjectOutputStream(new FileOutputStream("studenci.txt"));
			stream.writeObject(database);
			stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public StudentsDatabase load() {
		
		try {
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("studenci.txt"));
			StudentsDatabase database = (StudentsDatabase) inputStream.readObject();
			inputStream.close();
			return database;
		} catch (IOException | ClassNotFoundException e) {
			return new StudentsDatabase();
		}
	}
}
