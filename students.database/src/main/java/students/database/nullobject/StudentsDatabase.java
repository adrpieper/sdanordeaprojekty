package students.database.nullobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import students.database.NoSuchStudentException;
import students.database.Student;

public class StudentsDatabase implements Serializable {

	private Map<Long, Student> data = new HashMap<>();
	private long nextIndex = 100;

	public StudentsDatabase() {
	}

	public StudentsDatabase(List<Student> students) {
		long temp = 0;
		for (Student student : students) {
			if (student.getIndexNumber() > temp) {
				temp = student.getIndexNumber();
			}
			data.put(student.getIndexNumber(), student);
		}
		nextIndex = temp + 1;
	}

	public void add(String name, String surname) {
		data.put(nextIndex, new Student(nextIndex, name, surname));
		nextIndex++;
	}

	public List<Student> getAll() {
		return new ArrayList<>(data.values());
	}

	public Student get(long index) {
		if (!data.containsKey(index)) {
			return new Student(0,"Brak imienia", "Brak nazwiska");
		}
		return data.get(index);
	}

	public void remove(long index) {
		data.remove(index);
	}

}
