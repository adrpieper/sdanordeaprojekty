package students.database.nullobject;

public class StudentsNullObjectExample {

	public static void main(String[] args) {

		StudentsDatabase database = new StudentsDatabase();
		database.add("Jan", "Kowalski");

		System.out.println(database.get(100).getName());
		
		System.out.println(database.get(101).getName());
	}

}
