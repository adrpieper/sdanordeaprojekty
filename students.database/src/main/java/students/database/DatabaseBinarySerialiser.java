package students.database;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseBinarySerialiser implements DatabaseSerializer {

	@Override
	public void save(StudentsDatabase database) {
		try {
			DataOutputStream out = new DataOutputStream(new FileOutputStream("binarnie.txt"));
			for(Student student : database.getAll()) {
				out.writeUTF(student.getName());
				out.writeUTF(student.getSurname());
				out.writeLong(student.getIndexNumber());
			}
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public StudentsDatabase load() {
		try {
			DataInputStream in = new DataInputStream(new FileInputStream("binarnie.txt"));
			List<Student> students = new ArrayList<>();
			while(in.available() > 0) {
				String name = in.readUTF();
				String surname = in.readUTF();
				long id = in.readLong();
				students.add(new Student(id, name, surname));
			}
			return new StudentsDatabase(students);
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
