package students.database;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializationOfStudentExample {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub
		ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream("student.txt"));
		Student doZapisu = new Student(1000, "Adam", "Nowak");
		stream.writeObject(doZapisu);
		stream.close();
	}

}
