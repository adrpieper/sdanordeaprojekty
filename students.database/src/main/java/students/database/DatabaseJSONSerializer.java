package students.database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DatabaseJSONSerializer implements DatabaseSerializer {

	private ObjectMapper mapper = new ObjectMapper();
	
	@Override
	public void save(StudentsDatabase database) {
		try {
			FileOutputStream outputStream = new FileOutputStream("studenci.json");
			mapper.writeValue(outputStream, database.getAll());
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public StudentsDatabase load() {
		try {
			FileInputStream fileInputStream = new FileInputStream("studenci.json");
			List<Student> studenci = mapper.readValue(fileInputStream, new TypeReference<List<Student>>(){});
			fileInputStream.close();
			return new StudentsDatabase(studenci);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new StudentsDatabase();
	}

}
