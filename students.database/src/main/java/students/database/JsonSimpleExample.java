package students.database;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonSimpleExample {

	
	public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
		
		Student student = new Student(100, "Jan", "Kowalski");
		
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.writeValue(System.out, student);
		
		
	}
}
