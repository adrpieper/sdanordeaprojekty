package students.database;

import org.junit.Test;

public class StudentsDatabaseTest {

	@Test
	public void addStudentTest() {
		StudentsDatabase database = new StudentsDatabase();
		database.add("Jan", "Kolaski");
		database.add("Adam", "Kolaski");

		assert database.getAll().size() == 2;
	}
	
	@Test
	public void getStudentTest() {
		StudentsDatabase database = new StudentsDatabase();
		database.add("Jan", "Kolaski");
		database.add("Adam", "Kolaski");
		
		assert database.get(1).getName().equals("Adam");
	}
	
	@Test
	public void removetudentTest() {
		StudentsDatabase database = new StudentsDatabase();
		database.add("Jan", "Kolaski");
		database.add("Adam", "Kolaski");
		database.remove(0);
		
		assert database.getAll().size() == 1;
	}
	
}
