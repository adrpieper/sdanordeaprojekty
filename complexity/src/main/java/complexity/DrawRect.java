package complexity;

public class DrawRect {

	public static void main(String[] args) {
		drawRect(3);
		
	}

	public static void drawRect(int n) {
		StringBuilder str = new StringBuilder();
		for (int j = 0; j < n; j++) {
			str.append('*');
		}
		String stars = str.toString();

		for (int i = 0; i < n; i++) {
			System.out.println(stars);
		}
	}

}
