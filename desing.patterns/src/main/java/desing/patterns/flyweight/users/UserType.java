package desing.patterns.flyweight.users;

import java.util.Collection;
import java.util.EnumSet;

public class UserType {

	public final String name;
	public final EnumSet<Permission> permisions;
	
	public UserType(String name, Collection<Permission> permisions) {
		this.name = name;
		this.permisions = EnumSet.copyOf(permisions);
	}

	public EnumSet<Permission> getPermisions() {
		return permisions;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return "UserType [name=" + name + ", permisions=" + permisions + "]";
	}
	
}