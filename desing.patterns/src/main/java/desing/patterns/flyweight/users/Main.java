package desing.patterns.flyweight.users;

import static desing.patterns.flyweight.users.Permission.*;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		UserFactory factory = new UserFactory();

		factory.addType(new UserType("klient", Arrays.asList(CREATE_ORDERS)));
		factory.addType(new UserType("menager", Arrays.asList(Permission.values())));
		factory.addType(new UserType("sprzedawca", Arrays.asList(
				CREATE_PRODUCTS, 
				DELETE_PRODUCTS, 
				MODIFY_PRODUCTS,
				CREATE_ORDERS, 
				DELETE_ORDERS, 
				MODIFY_ORDERS)));
	
		User user = factory.create("Edek", "123", "klient");
		System.out.println(user);

		User seller = factory.create("Edek", "123", "sprzedawca");
		System.out.println(seller.getType().getPermisions());
	}

}
