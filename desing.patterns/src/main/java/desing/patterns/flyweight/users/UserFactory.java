package desing.patterns.flyweight.users;

import java.util.HashMap;
import java.util.Map;

public class UserFactory {
	private Map<String, UserType> usersTypes = new HashMap<>();
	
	public void addType(UserType userType) {
		usersTypes.put(userType.getName(), userType);
	}

	public User create(String nickname, String password, String type) {	
		return new User(nickname, password, usersTypes.get(type));
	}
}
