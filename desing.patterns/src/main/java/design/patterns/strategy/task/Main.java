package design.patterns.strategy.task;

import java.util.PriorityQueue;
import java.util.Queue;

public class Main {

	public static void main(String[] args) {


		ExecutionStrategy executor = new MultiThreadExecutor();

		executor.add(new Counting(1));
		executor.add(new Counting(1));
		executor.add(new Counting(2));
		executor.add(new Counting(2));
		executor.add(new Counting(1));

		executor.add(new Counting(1));
		executor.add(new Counting(1));
		executor.add(new Counting(2));
		executor.add(new Counting(2));
		executor.add(new Counting(1));

		executor.add(new Counting(1));
		executor.add(new Counting(1));
		executor.add(new Counting(2));
		executor.add(new Counting(2));
		executor.add(new Counting(1));
		
		executor.executeAll();
	}

}
