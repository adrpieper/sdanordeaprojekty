package design.patterns.strategy.task;


public class NoQueueExecutor implements ExecutionStrategy {

	@Override
	public void add(Task task) {
		task.execute();
	}

	@Override
	public void executeAll() {

	}
}
