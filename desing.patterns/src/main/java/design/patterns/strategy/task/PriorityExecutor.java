package design.patterns.strategy.task;

import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityExecutor implements ExecutionStrategy {
	private Queue<Task> tasks = new PriorityQueue<>();
	
	@Override
	public void add(Task task) {
		tasks.offer(task);
	}
	
	@Override
	public void executeAll() {
		while(!tasks.isEmpty()) {
			Task task = tasks.poll();
			System.out.println(task.getName() + " (" + task.getPriority() + ")");
			task.execute();
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}
