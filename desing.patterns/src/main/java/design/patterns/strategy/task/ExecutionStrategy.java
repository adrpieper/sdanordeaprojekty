package design.patterns.strategy.task;

public interface ExecutionStrategy {

	void add(Task task);

	void executeAll();

}