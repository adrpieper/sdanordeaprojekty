package design.patterns.strategy.task;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MultiThreadExecutor implements ExecutionStrategy {
	private Queue<Task> tasks = new LinkedList<>();
	private Executor executor = Executors.newFixedThreadPool(10);
	
	@Override
	public void add(Task task) {
		tasks.offer(task);
	}
	
	@Override
	public void executeAll() {
		while(!tasks.isEmpty()) {
			Task task = tasks.poll();
			executor.execute(new Runnable() {
				
				@Override
				public void run() {
					System.out.println(task.getName() + " (" + task.getPriority() + ")");
					task.execute();
					
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});
			
		}
	}
	
}
