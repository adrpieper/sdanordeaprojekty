package design.patterns.strategy.task;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MultiThreadExample {

	public static void main(String[] args) {
		
		
		Executor executor = Executors.newFixedThreadPool(3);
		
		for (int i = 0; i < 10; i++) {
			executor.execute(new Task());
		}
		
	}
	
	static class Task implements Runnable {
		
		@Override
		public void run() {
			for(int i = 0 ; i < 5 ; i++) {
				System.out.println(i);
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
