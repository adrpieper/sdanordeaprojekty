package design.patterns.strategy.task;

public class DolphinTask extends Task {

	public DolphinTask() {
		super("Dolphin");
	}

	@Override
	public void execute() {
		System.out.println("                               _.-~~.)");
		System.out.println("         _.--~~~~~---....__  .' . .,' ");
		System.out.println("       ,'. . . . . . . . . .~- ._ (");
		System.out.println("      ( .. .g. . . . . . . . . . .~-._");
		System.out.println("   .~__.-~    ~`. . . . . . . . . . . -. ");
		System.out.println("   `----..._      ~-=~~-. . . . . . . . ~-.  ");
		System.out.println("             ~-._   `-._ ~=_~~--. . . . . .~.  ");
		System.out.println("              | .~-.._  ~--._-.    ~-. . . . ~-.");
		System.out.println("                .(   ~~--.._~'       `. . . . .~-.                ,");
		System.out.println("                `._         ~~--.._    `. . . . . ~-.    .- .   ,'/");
		System.out.println("_  . _ . -~        _ ..  _          ~~--.`_. . . . . ~-_     ,-','`  .");
		System.out.println("             ` ._           ~                ~--. . . . .~=.-'. /. `");
		System.out.println("       - . -~            -. _ . - ~ - _   - ~     ~--..__~ _,. /     - ~");
		System.out.println("              . __ ..                   ~-               ~~_. (  `");
		System.out.println(")`. _ _               `-       ..  - .    . - ~ ~ .        ~-` ` `  `. _");
	}
}
