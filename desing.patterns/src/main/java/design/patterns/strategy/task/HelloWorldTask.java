package design.patterns.strategy.task;

public class HelloWorldTask extends Task {

	public HelloWorldTask(int priority) {
		super(priority, "HelloWorldTask");
	}

	@Override
	public void execute() {
		System.out.println("Hello World!");
	}

}
