package design.patterns.composite.company;

import java.util.LinkedList;
import java.util.List;

public class Manager implements Worker {

	private String name;
	private List<Worker> workers = new LinkedList<>();
	
	public Manager(String name) {
		this.name = name;
	}

	@Override
	public void introduce() {
		System.out.println("Nazywam si� " + name + " jestem pracownikiem : {");
		
		for(Worker worker : workers) {
			worker.introduce();
		}
		
		System.out.println("}");
	}
	
	public void add(Worker worker) {
		workers.add(worker);
	}
}
