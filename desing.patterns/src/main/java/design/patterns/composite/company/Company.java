package design.patterns.composite.company;

public class Company {

	public static void main(String[] args) {
		
		Manager president = new Manager("Andrzej");
		Worker w1 = new SimpleWorker("Jan", "Programista");
		Worker w2 = new SimpleWorker("Janina", "Administrator");
		Manager m = new Manager("J�zef");
		Worker w3 = new SimpleWorker("Damian", "Wo�ny");

		president.add(w1);
		president.add(w2);
		president.add(m);
		m.add(w3);
		
		m.introduce();
	}
}
