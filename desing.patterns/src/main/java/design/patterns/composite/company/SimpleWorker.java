package design.patterns.composite.company;

public class SimpleWorker implements Worker {
    
    private String name;
    private String function;
    
    public SimpleWorker(String name, String function) {

		this.name = name;
		this.function = function;
	}

    @Override
    public void introduce() {
        System.out.println("Nazywam si� " + name + " (" + function + ").");
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getFunction() {
        return function;
    }
    public void setFunction(String function) {
        this.function = function;
    }
    
}
