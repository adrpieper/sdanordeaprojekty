package design.patterns.composite.company;

public interface Worker {
	void introduce();
}
