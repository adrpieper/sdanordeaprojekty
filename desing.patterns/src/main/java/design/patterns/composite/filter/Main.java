package design.patterns.composite.filter;

public class Main {

	
	private static void print(Filter filter, int[] numbers) {
		
		for(int i : numbers) {
			if (filter.check(i)) {
				System.out.println(i);
			}
		}
	}
	
	public static void main(String[] args) {

		int [] numbers = {1,2,3,4,5,6,7,8,9,10};

		System.out.println("Wi�ksze ni� 3");
		Filter moreThen3 = new Filter() {
			
			@Override
			public boolean check(int number) {
				return number > 3;
			}
		};
		
		print(moreThen3, numbers);
		
		System.out.println("Podzielne przez 2");
		
		print(new Filter() {
			
			@Override
			public boolean check(int number) {
				return number%2 == 0;
			}
		}, numbers);
		
		System.out.println("Podzielne przez 4");
		print(number -> number%4 == 0, numbers);
		
		System.out.println("Wi�ksze ni� 6");
		print(Main::moreThen6, numbers);
		
		Filter divideBy2 = new Filter() {
			
			@Override
			public boolean check(int number) {
				return number%2 == 0;
			}
		};

		System.out.println("Wi�ksze ni� 3, podzielne przez 2");
		print(new AndFilter(moreThen3, divideBy2), numbers);
		
	}
	
	public static boolean moreThen6(int number) {
		return number > 6;
	}

}
