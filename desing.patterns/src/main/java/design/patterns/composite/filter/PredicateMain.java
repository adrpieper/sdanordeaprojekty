package design.patterns.composite.filter;

import java.util.Arrays;
import java.util.function.Predicate;

public class PredicateMain {

	public static void main(String[] args) {


		Predicate<String> p1 = (s) -> s.length() < 5;
		Predicate<String> p2 = new Predicate<String>() {
			
			@Override
			public boolean test(String t) {
				return t.length() < 5;
			}
		};
		System.out.println(p1.test("Adi"));
		System.out.println(p2.test("Adi12345"));
		
		
		Arrays.asList(1,2,3,4,5,6,7,8,9).stream().filter(i -> i < 6).forEach(System.out::println);
		
		int number = Arrays.asList(1,2,3,4,5,6,7,8,9).stream()
				.mapToInt(i -> i)
				.max()
				.getAsInt();		
		
		System.out.println(number);
	}

}
