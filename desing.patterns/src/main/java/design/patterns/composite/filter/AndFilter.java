package design.patterns.composite.filter;

public class AndFilter implements Filter {
	
	private Filter[] filters;

	public AndFilter(Filter... filters) {
		this.filters = filters;
	}

	@Override
	public boolean check(int number) {
		
		for(Filter filter : filters) {
			if (!filter.check(number)) {
				return false;
			}
		}
		return true;
	}
	
	

}
