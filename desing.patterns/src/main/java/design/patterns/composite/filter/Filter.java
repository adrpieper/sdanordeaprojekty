package design.patterns.composite.filter;

public interface Filter {
	boolean check(int number);
}
