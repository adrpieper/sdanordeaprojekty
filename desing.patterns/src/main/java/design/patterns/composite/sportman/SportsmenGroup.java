package design.patterns.composite.sportman;

import java.util.LinkedList;
import java.util.List;

import design.patterns.decorator.sportman.Sportsman;

public class SportsmenGroup implements Sportsman {

	private List<Sportsman> sportsmen = new LinkedList<>();
	
	public SportsmenGroup(Sportsman... sportsmen) {
		for (Sportsman sportsman : sportsmen) {
			this.sportsmen.add(sportsman);
		}
	}
	
	@Override
	public void prepare() {
		for(Sportsman sportsman : sportsmen) {
			sportsman.prepare();
		}
	}

	@Override
	public void doPumps(int number) {
		for(Sportsman sportsman : sportsmen) {
			sportsman.doPumps(number);
		}
	}

	@Override
	public void doSquats(int number) {
		for(Sportsman sportsman : sportsmen) {
			sportsman.doSquats(number);
		}
	}

	@Override
	public void doCrunches(int number) {
		for(Sportsman sportsman : sportsmen) {
			sportsman.doCrunches(number);
		}
	}

}
