package design.patterns.composite.sportman;

import design.patterns.decorator.sportman.BasicSportsMan;
import design.patterns.decorator.sportman.FitnessStudio;
import design.patterns.decorator.sportman.NoPrepare;
import design.patterns.decorator.sportman.Sportsman;
import design.patterns.decorator.sportman.WaterDrinking;

public class Main {

	public static void main(String[] args) {

		Sportsman s1 = new BasicSportsMan();
		Sportsman s2 = new WaterDrinking(new BasicSportsMan());
		Sportsman s3 = new NoPrepare(new BasicSportsMan());
		
		Sportsman group = new SportsmenGroup(s1,s2,s3);
		
		FitnessStudio fitnessStudio = new FitnessStudio();
		
		fitnessStudio.train(group);
		
	}

}
