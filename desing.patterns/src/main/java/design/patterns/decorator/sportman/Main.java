package design.patterns.decorator.sportman;

public class Main {

	public static void main(String[] args) {

		FitnessStudio fitnessStudio = new FitnessStudio();
		Sportsman sportsman = new WaterDrinking(new NoPrepare(new BasicSportsMan()));
		fitnessStudio.train(sportsman); 
	}


}
