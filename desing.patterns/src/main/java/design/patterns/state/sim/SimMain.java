package design.patterns.state.sim;

public class SimMain {

	public static void main(String[] args) {

		Sim sim = new Sim();

		sim.work();
		sim.work();

		sim.rest();
		sim.work();
	}

}
