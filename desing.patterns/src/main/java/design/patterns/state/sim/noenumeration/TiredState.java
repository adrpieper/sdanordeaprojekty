package design.patterns.state.sim.noenumeration;

class TiredState implements State {
	@Override
	public void work() {
		System.out.println("Jestem zm�czony, nie b�d� pracowa�.");
	}
}