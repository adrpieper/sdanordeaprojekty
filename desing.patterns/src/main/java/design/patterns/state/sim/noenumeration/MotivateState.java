package design.patterns.state.sim.noenumeration;

public class MotivateState implements State {
	@Override
	public void work() {
		System.out.println("Pracuje bardzo wydajnie");
	}
}