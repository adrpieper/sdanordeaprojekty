package design.patterns.state.sim.noenumeration;

public interface State {
	void work();
}
