package design.patterns.state.sim.noenumeration;

public class Sim {

	private State state;

	private static final State RESTED = new RestedState();
	
	private static final State TIRED = new TiredState();
	
	private static final State MOTIVATE = new MotivateState();
	
	public Sim() {
		this.state = RESTED;
	}

	public void rest() {
		System.out.println("Odpoczywam..");
		state = RESTED;
	}

	public void work() {
		state.work();
		state = TIRED;
	}
	
	public void reward() {
		state = MOTIVATE;
	}
}
