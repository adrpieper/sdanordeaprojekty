package design.patterns.flyweight.shop;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main2 {

	public static void main(String[] args) {
		Shop shop = new Shop();
		shop.addProduct(new Product("Mas�o", 5, "Bardzo smaczne"));
		shop.addProduct(new Product("Chleb", 2, "Chleb Pe�noziarnisty"));
		shop.addProduct(new Product("M�ka", 2, "Pszenna"));
	
		Random r = new Random();
		List<Product> products = new ArrayList<>();
		
		String[] names = {"Mas�o","Chleb","M�ka"};
		for(int i = 0 ; i < 1000 ; i++) {
			String name = names[r.nextInt(3)]; // losuj� element tablicy
			Product product = shop.getProduct(name); // pobieram produkt ze sklepu
			products.add(product);
		}
		
		for(Product p : products) {
			System.out.println(p.getName() + " - " + p.getPrice() + "z�");
		}
	}

}
