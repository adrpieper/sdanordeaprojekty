package design.patterns.flyweight.shop;

import java.util.LinkedList;
import java.util.List;

public class Main {

	public static void main(String[] args) {


		List<Product> products = new LinkedList<>();
		Product product = new Product("Mas�o", 5, "Bardzo pyszne.");
		for(int i = 0 ; i < 1000; i++) {
			products.add(product);
		}
		
		
		/*
		 * List<Product> products = new LinkedList<>();
		 * for(int i = 0 ; i < 1000; i++) {
		 *     products.add(new Product("Mas�o", 5, "Bardzo pyszne"));
		 * }
		 */
		
	}

}
