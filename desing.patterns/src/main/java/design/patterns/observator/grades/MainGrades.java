package design.patterns.observator.grades;

public class MainGrades {

	
	public static void main(String[] args) {
		
		Grades grades = new Grades();
		
		GradesObserver randomObserver = new GradesObserver() {
			
			@Override
			public void newGrade(int grade) {
				System.out.println("O, jaka� ocena " + grade );
			}
		};
		
		grades.addObserver(randomObserver);

		grades.addGrade(1);
		grades.addGrade(3);
		grades.addGrade(4);
		grades.addGrade(5);
		
	}
}
