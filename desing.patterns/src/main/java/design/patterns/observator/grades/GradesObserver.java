package design.patterns.observator.grades;

public interface GradesObserver {
	void newGrade(int grade);
}
