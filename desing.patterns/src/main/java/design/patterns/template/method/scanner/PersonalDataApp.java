package design.patterns.template.method.scanner;

import java.util.Scanner;

public class PersonalDataApp extends StandartConsoleAppTempleteMethod {

	private String name;
	private String surname;
	
	@Override
	public void showResult() {
		System.out.println("Imi� : " + name);
		System.out.println("Nazwisko : " + surname);
	}

	@Override
	public void calculateResult() {}

	@Override
	public void getData(Scanner scanner) {
		System.out.println("Podaj imi�");
		name = scanner.next();
		System.out.println("Podaj nazwisko");
		surname = scanner.next();

	}

	public static void main(String[] args) {
		new PersonalDataApp().main();
	}

}
