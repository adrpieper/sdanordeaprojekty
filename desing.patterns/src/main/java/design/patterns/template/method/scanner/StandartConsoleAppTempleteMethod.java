package design.patterns.template.method.scanner;

import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class StandartConsoleAppTempleteMethod {

	public void main() {
		Scanner scanner = new Scanner(System.in);
		boolean validData = false;
		do {
			try{
				getData(scanner);
				validData = true;
			}catch (InputMismatchException exception) {
				System.out.println("Podaj dane jeszcze raz");
				scanner.reset();
				scanner.next(); 
			}
		}while(!validData);
		calculateResult();
		showResult();
	}

	public abstract void showResult();
	public abstract void calculateResult();
	public abstract void getData(Scanner scanner);
}
