package design.patterns.template.method.scanner;

import java.util.Scanner;

public class Maximum extends StandartConsoleAppTempleteMethod {
	private int first;
	private int second;
	private int result;
	
	@Override
	public void showResult() {
		// TODO Auto-generated method stub
		System.out.println("max value: " + result);
	}

	@Override
	public void calculateResult() {
		// TODO Auto-generated method stub
		result = first < second ? second : first;
	}

	@Override
	public void getData(Scanner scanner) {
		// TODO Auto-generated method stub
		System.out.println("type first number");
		first = scanner.nextInt();
		System.out.println("type second number");
		second = scanner.nextInt();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Maximum().main();
	}

}
