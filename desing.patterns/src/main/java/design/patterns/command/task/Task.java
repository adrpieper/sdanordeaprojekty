package design.patterns.command.task;

public abstract class Task implements Comparable<Task>{
	private final int priority;
	private final String name;
	
	public Task(String name) {
		this(1, name);
	}
	
	public Task(int priority, String name) {
		this.priority = priority;
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public int getPriority() {
		return priority;
	}

	public abstract void execute();
	
	@Override
	public int compareTo(Task o) {
		return o.priority - priority;
	}
}
