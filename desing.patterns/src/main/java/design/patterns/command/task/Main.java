package design.patterns.command.task;

import java.util.PriorityQueue;
import java.util.Queue;


public class Main {

	public static void main(String[] args) {

		Queue<Task> tasks = new PriorityQueue<>();
		
		tasks.offer(new HelloWorldTask(1));
		tasks.offer(new HelloWorldTask(2));
		tasks.offer(new HelloWorldTask(2));
		tasks.offer(new DolphinTask());
		
		while(!tasks.isEmpty()) {
			Task task = tasks.poll();
			System.out.println(task.getName() + " (" + task.getPriority() + ")");
			task.execute();
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
