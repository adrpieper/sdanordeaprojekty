package design.patterns.prototype.users;

import java.util.HashMap;
import java.util.Map;

public class UserFactory {
	private Map<String, User> usersPrototypes = new HashMap<>();
	
	public void addPrototype(User user) {
		usersPrototypes.put(user.getKind(), user);
	}

	
	public User create(String nickname, String password, String kind) {
		
		try {
			User prototype = usersPrototypes.get(kind);
			User user = prototype.clone();
			user.setNickname(nickname);
			user.setPassword(password);
			return user;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
