package design.patterns.prototype.users;

import static design.patterns.prototype.users.Permission.*;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		UserFactory factory = new UserFactory();

		factory.addPrototype(new User("", "", "klient", Arrays.asList(CREATE_ORDERS)));
		factory.addPrototype(new User("", "", "menager", Arrays.asList(Permission.values())));
		factory.addPrototype(new User("", "", "sprzedawca", Arrays.asList(
				CREATE_PRODUCTS, 
				DELETE_PRODUCTS, 
				MODIFY_PRODUCTS,
				CREATE_ORDERS, 
				DELETE_ORDERS, 
				MODIFY_ORDERS)));
	
		User user = factory.create("Edek", "123", "klient");
		System.out.println(user);

		User seller = factory.create("Edek", "123", "sprzedawca");
		System.out.println(seller.getPermisions());
	}

}
