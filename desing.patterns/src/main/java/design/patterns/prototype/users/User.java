package design.patterns.prototype.users;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

public class User {
	private String nickname;
	private String password;
	private String kind;
	private EnumSet<Permission> permisions;
	
	public User(String nickname, String password, String kind, Collection<Permission> permisions) {
		this.nickname = nickname;
		this.password = password;
		this.kind = kind;
		this.permisions = EnumSet.copyOf(permisions);
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public Set<Permission> getPermisions() {
		return permisions;
	}

	public void setPermisions(Collection<Permission> permisions) {
		this.permisions = EnumSet.copyOf(permisions);
	}

	@Override
	public String toString() {
		return "User [nickname=" + nickname + ", password=" + password + ", kind=" + kind + ", permisions=" + permisions
				+ "]";
	}

	@Override
	protected User clone() throws CloneNotSupportedException {
		return new User(nickname, password, kind, permisions);
	}
	
	
	
}
